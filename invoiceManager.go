package cetustekinvoice

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"github.com/beevik/etree"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func CreateInvoice(params CreateInvoiceParams) (response []string,err error)  {

	if !CheckConnection(params){
		log.Println("Connection failed. ", err)
		return
	}

	payload:=CreateInvoiceTemplate(params)

	req, err := http.NewRequest(params.HttpMethod,params.Host,bytes.NewReader(payload))
	if err != nil {
		log.Println("Error on creating request object. ", err)
		return
	}

	req.Header.Set("Content-type", "text/xml")

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	res, err := client.Do(req)
	if err != nil {
		log.Println("Error on dispatching request. ", err)
		return
	}

	result := new(CreateInvoiceV3Response)
	err = xml.NewDecoder(res.Body).Decode(result)

	if err !=nil{
		log.Println("Error on xml. ", err)
		return
	}

	invoice := result.Body.CreateInvoiceV3.Return

	return invoice,nil
}

func CheckConnection(params CreateInvoiceParams)bool {
	response, errors := http.Get(params.Host)

	if errors != nil {
		_, netErrors := http.Get("https://www.google.com")

		if netErrors != nil {
			fmt.Fprintf(os.Stderr, "no internet\n")
			os.Exit(1)
		}
		return false
	}

	if response.StatusCode == 200 {
		return true
	}

	return false
}

type CreateInvoiceParams struct {
	Host string
	HttpMethod string
	InvoiceXml string
	HasTax string
	RentId string
	Source Source
}

type Source struct {
	WebCode string
	APIPassword string
}

type CreateInvoiceV3Response struct {
	XMLName xml.Name
	Body    struct {
		XMLName           xml.Name
		CreateInvoiceV3 struct {
			XMLName xml.Name
			Return  []string `xml:"return"`
		} `xml:"CreateInvoiceV3Response"`
	}
}

func CreateInvoiceTemplate(params CreateInvoiceParams) []byte  {
	source:=params.Source.WebCode+params.Source.APIPassword
	payload:=[]byte(strings.TrimSpace(`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.cetustek.com/">
    <soapenv:Header/>
    <soapenv:Body>
      <web:CreateInvoiceV3>
         <invoicexml><![CDATA[`+params.InvoiceXml+`]]></invoicexml>
         <hastax>`+params.HasTax+`</hastax>
         <rentid>`+params.RentId+`</rentid>
         <source>`+source+`</source>
      </web:CreateInvoiceV3>
    </soapenv:Body>
    </soapenv:Envelope>`))
	return payload
}

type ProductItem struct {
	ProductCode string `json:"ProductCode"`
	Description string `json:"Description"`
	Quantity    int    `json:"Quantity"`
	Unit        string `json:"Unit"`
	UnitPrice   int    `json:"UnitPrice"`
}

type OrderInfo struct {
	OrderId              string `json:"OrderId"`
	OrderDate            string `json:"OrderDate"`
	BuyerIdentifier      string `json:"BuyerIdentifier"`
	BuyerName            string `json:"BuyerName"`
	BuyerAddress         string `json:"BuyerAddress"`
	BuyerPersonInCharge  string `json:"BuyerPersonInCharge"`
	BuyerTelephoneNumber string `json:"BuyerTelephoneNumber"`
	BuyerFacsimileNumber string `json:"BuyerFacsimileNumber"`
	BuyerEmailAddress    string `json:"BuyerEmailAddress"`
	BuyerCustomerNumber  string `json:"BuyerCustomerNumber"`
	DonateMark           int    `json:"DonateMark"`
	InvoiceType          string `json:"InvoiceType"`
	CarrierType          string `json:"CarrierType"`
	CarrierId1           string `json:"CarrierId1"`
	CarrierId2           string `json:"CarrierId2"`
	NPOBAN               string `json:"NPOBAN"`
	TaxType              string `json:"TaxType"`
	TaxRate              string `json:"TaxRate"`
	PayWay               string `json:"PayWay"`
	Remark               string `json:"Remark"`
	MailSend             string `json:"MailSend"`
}

func SetOrderXml(orderInfo OrderInfo,productItems []ProductItem) string  {
	doc := etree.NewDocument()
	doc.CreateProcInst("xml", `version="1.0" encoding="utf-8"`)

	invoice:=doc.CreateElement("Invoice")
	invoice.CreateAttr("XSDVersion","2.8")

	orderId := invoice.CreateElement("OrderId")
	orderId.SetText(orderInfo.OrderId)

	orderDate := invoice.CreateElement("OrderDate")
	orderDate.SetText(orderInfo.OrderDate)

	buyerIdentifier := invoice.CreateElement("BuyerIdentifier")
	buyerIdentifier.SetText(orderInfo.BuyerIdentifier)

	buyerName := invoice.CreateElement("BuyerName")
	buyerName.SetText(orderInfo.BuyerName)

	buyerAddress := invoice.CreateElement("BuyerAddress")
	buyerAddress.SetText(orderInfo.BuyerAddress)

	buyerPersonInCharge := invoice.CreateElement("BuyerPersonInCharge")
	buyerPersonInCharge.SetText(orderInfo.BuyerPersonInCharge)

	buyerTelephoneNumber := invoice.CreateElement("BuyerTelephoneNumber")
	buyerTelephoneNumber.SetText(orderInfo.BuyerTelephoneNumber)

	buyerFacsimileNumber := invoice.CreateElement("BuyerFacsimileNumber")
	buyerFacsimileNumber.SetText(orderInfo.BuyerFacsimileNumber)

	buyerEmailAddress := invoice.CreateElement("BuyerEmailAddress")
	buyerEmailAddress.SetText(orderInfo.BuyerEmailAddress)

	buyerCustomerNumber := invoice.CreateElement("BuyerCustomerNumber")
	buyerCustomerNumber.SetText(orderInfo.BuyerCustomerNumber)

	donateMark := invoice.CreateElement("DonateMark")
	donateMark.SetText(strconv.Itoa(orderInfo.DonateMark))

	invoiceType := invoice.CreateElement("InvoiceType")
	invoiceType.SetText(orderInfo.InvoiceType)

	carrierType := invoice.CreateElement("CarrierType")
	carrierType.SetText(orderInfo.CarrierType)

	carrierId1 := invoice.CreateElement("CarrierId1")
	carrierId1.SetText(orderInfo.CarrierId1)

	carrierId2 := invoice.CreateElement("CarrierId2")
	carrierId2.SetText(orderInfo.CarrierId2)

	NPOBAN := invoice.CreateElement("NPOBAN")
	NPOBAN.SetText(orderInfo.NPOBAN)

	taxType := invoice.CreateElement("TaxType")
	taxType.SetText(orderInfo.TaxType)

	taxRate := invoice.CreateElement("TaxRate")
	taxRate.SetText(orderInfo.TaxRate)

	payWay := invoice.CreateElement("PayWay")
	payWay.SetText(orderInfo.PayWay)

	remark := invoice.CreateElement("Remark")
	remark.SetText(orderInfo.Remark)

	details := invoice.CreateElement("Details")
	for i:=0;i<len(productItems);i++ {
		productItem := details.CreateElement("ProductItem")

		productionCode := productItem.CreateElement("ProductionCode")
		productionCode.SetText(productItems[i].ProductCode)

		description := productItem.CreateElement("Description")
		description.SetText(productItems[i].Description)

		quantity := productItem.CreateElement("Quantity")
		quantity.SetText(strconv.Itoa(productItems[i].Quantity))

		unit := productItem.CreateElement("Unit")
		unit.SetText(productItems[i].Unit)

		unitPrice := productItem.CreateElement("UnitPrice")
		unitPrice.SetText(strconv.Itoa(productItems[i].UnitPrice))
	}
	result, _ := doc.WriteToString()

	return result
}

func GetInvoiceNumber(response []string)(number string, success bool) {
	result:=strings.Split(response[0],";")
	if len(result)==1{
		fmt.Println("error code :",result[0])
		return result[0],false
	}
	if len(result)==2{
		fmt.Println("number :",result[0])
		return result[0],true
	}
	return "",false
}