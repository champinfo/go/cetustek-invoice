module git.championtek.com.tw/go/cetustekinvoice

go 1.14

require (
	github.com/beevik/etree v1.1.0
	gopkg.in/yaml.v2 v2.3.0
)
