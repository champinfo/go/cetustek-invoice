package config

import (
	"encoding/json"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

var SysConfig = &config{}

type config struct {
	Getustek      getustek             `json:"Getustek" yaml:"Getustek"`
}

type getustek struct {
	User     string `json:"user" yaml:"User"`
	Password string `json:"password" yaml:"Password"`
	Host     string `json:"host" yaml:"Host"`
}

func init() {
	_, filePath, _, _ := runtime.Caller(0)
	_ = filepath.Walk(path.Dir(filePath), func(path string, info os.FileInfo, err error) error {
		if strings.Index(info.Name(), "config") != -1 {
			ReadSysConfig(filePath, info.Name())
		}
		return nil
	})
}

func ReadSysConfig(Path string, FileName string) bool {
	if b, err := ioutil.ReadFile(path.Join(path.Dir(Path), FileName)); !os.IsNotExist(err) {
		switch filepath.Ext(FileName) {
		case ".yaml":
			err = yaml.Unmarshal(b, &SysConfig)
		case ".json":
			err = json.Unmarshal(b, &SysConfig)
		}
		if err != nil {
			log.Fatalf("error: %v", err)
		}
		return true
	}
	return false
}